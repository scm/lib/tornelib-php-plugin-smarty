<?php

/**
 * Copyright 2017 Tomas Tornevall & Tornevall Networks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package TorneLIB
 * @version 6.0.0
 */

namespace TorneLIB;

if (file_exists("../vendor/autoload.php")) {require_once( '../vendor/autoload.php' );}

/**
 * TorneLIB Smarty Library Utilizer
 * Written for backward compatibility only.
 * Tested with 3.1.28 and 3.1.31
 *
 * Why this dual class? Back in the days TorneLIB had its own way to handle templates, inspired by a very old vBulletin release.
 * This code could however never release, since there was a copyright issue in the code of vBulletin. When we got aware of Smarty
 * the old function "evaltemplate" was replaced by the Smarty edition. To maintain the compatibility, Smarty became the default
 * template handler for the global TorneLIB project, which still supported $Library->evaltemplate(); in most of the code.
 *
 * Class TorneLIB_Smarty
 * @package TorneLIB
 * @version 6.0.0
 */
class TorneLIB_Plugin_Smarty {

	/** @var \Smarty */
	private $PLUGIN;
	/** @var string Internal */
	private $template_dir = "";
	/** @var bool Setting for always cleaning up cache and templates afterwards - keeping our storage clean */
	private $doCleanup = true;
	/** @var bool Internal protective */
	private $enforceCleanup = false;
	 /** @var  Smarty tmp path */
	private $smartyInternalTemporaryPath;

	/**
	 * TorneLIB_Smarty constructor.
	 *
	 * @param string $PathLoader
	 */
	function __construct( $PathLoader = '' ) {
		$this->PLUGIN      = new \Smarty();
		$smartyTemplateDir = $this->PLUGIN->getTemplateDir();
		if ( count( $smartyTemplateDir ) > 0 ) {
			$this->template_dir = preg_replace( "/\/$/", '', array_pop( $smartyTemplateDir ) );
		}
		// By default we don't want to clean up caches
		$this->setCleanUp( false );

		// Prepare initial cache storage
		$this->setCache();
	}

	/**
	 * Clean up cache and compiled store
	 *
	 * @param bool $forceMe
	 *
	 * @since 6.0.0
	 */
	public function cleanup( $forceMe = false ) {
		if ( $this->doCleanup || $forceMe || $this->enforceCleanup ) {
			$this->PLUGIN->clearCompiledTemplate();
			$compileDirectory = $this->PLUGIN->getCompileDir();
			$cacheDirectory   = $this->PLUGIN->getCacheDir();
			if ( file_exists( $compileDirectory ) ) {
				@rmdir( $compileDirectory );
			}
			if ( file_exists( $cacheDirectory ) ) {
				@rmdir( $cacheDirectory );
			}
			if ( !empty($this->smartyInternalTemporaryPath) && file_exists($this->smartyInternalTemporaryPath)) {
				@rmdir($this->smartyInternalTemporaryPath);
			}
		}
	}

	/**
	 * Set own cache-compile store
	 *
	 * This function automatically protects cached data by erasing it, if "know public temp directories" are used.
	 *
	 * @param string $compileAndCachePath
	 * @since 6.0.0
	 */
	public function setCache( $compileAndCachePath = "/tmp/smarty" ) {
		$compileAndCachePath = preg_replace( "/\/$/", '', $compileAndCachePath );
		// Always enforce cleanup when using /tmp since that used to be publicly available
		$tmp = array( "/tmp", "/var/tmp", "/usr/tmp", "/usr/local/tmp" );
		if ( isset( $_SERVER['DOCUMENT_ROOT'] ) && ! empty( $_SERVER['DOCUMENT_ROOT'] ) ) {
			$tmp[] = $_SERVER['DOCUMENT_ROOT'];
		}
		if (!$this->enforceCleanup) {
			foreach ( $tmp as $criticalTmpPath ) {
				if ( preg_match( "@" . $criticalTmpPath . "@i", $compileAndCachePath ) ) {
					$this->enforceCleanup = true;
				}
			}
		}
		if ( in_array( $compileAndCachePath, $tmp ) && ! $this->enforceCleanup ) {
			$this->enforceCleanup = true;
		}
		$this->smartyInternalTemporaryPath = $compileAndCachePath;
		$this->PLUGIN->setCompileDir( $compileAndCachePath . "/compile" )->setCacheDir( $compileAndCachePath . "/cache" );
	}

	/**
	 * @param bool $activateBool
	 *
	 * @since 6.0.0
	 */
	public function setCleanUp( $activateBool = true ) {
		$this->doCleanup = $activateBool;
	}

	/**
	 * Return cleanup flag
	 * @return bool
	 * @since 6.0.0
	 */
	public function getCleanUp() {
		return $this->doCleanup;
	}

	/**
	 * Smarty call wrapper
	 *
	 * @param $name
	 * @param $arguments
	 *
	 * @return mixed
	 * @throws \Exception
	 * @since 5.0.0
	 */
	function __call( $name, $arguments ) {
		$returnedCall = null;
		try {
			if ( method_exists( $this->PLUGIN, $name ) ) {
				$returnedCall = @call_user_func_array( array( $this->PLUGIN, $name ), $arguments );
				$this->cleanup();
			} else {
				throw new \Exception( "Exception " . __CLASS__ . "\\" . $name . ": Not a method", 404 );
			}
		} catch ( \Exception $e ) {
			throw new \Exception( "Exception " . __CLASS__ . "\\" . $name . ": " . $e->getMessage(), $e->getCode(), $e );
		}

		return $returnedCall;
	}

	/**
	 * TornevallWEB v4 Function Clone
	 *
	 * @param string $fileName
	 * @param array $variableArray
	 * @param bool $isFile
	 *
	 * @return null|void
	 * @throws \Exception
	 * @since 5.0.0
	 */
	public function evalTemplate( $fileName = '', $variableArray = array(), $isFile = true ) {
		$fileName       = preg_replace( "/\/$/i", '', $fileName );
		$RealFileName   = $fileName;
		$returnedString = null;
		/* Silently suppress everything passed here, except for catchable errors */
		try {
			if ( is_array( $variableArray ) ) {
				$this->PLUGIN->assign( $variableArray );
			}
			if ( file_exists( $fileName ) ) {
				$RealFileName = $fileName;
			}
			if ( ! file_exists( $RealFileName ) && ! empty( $this->template_dir ) && file_exists( $this->template_dir . "/" . $fileName ) ) {
				$RealFileName = $this->template_dir . "/" . $fileName;
			}
			if ( $isFile ) {
				if ( file_exists( $RealFileName ) ) {
					$returnedString = $this->PLUGIN->fetch( $RealFileName );
					$this->cleanup();

					return $returnedString;
				} else {
					$protectedTemplateInfo = pathinfo( $fileName );
					throw new \Exception( "Exception " . __CLASS__ . "\\" . __FUNCTION__ . ": " . "Template '" . $protectedTemplateInfo['filename'] . "' does not exist", 404 );
				}
			} else {
				$compileDir = preg_replace( "/\/$/", '', $this->PLUGIN->compile_dir );
				if ( ! empty( $compileDir ) && file_exists( $this->PLUGIN->compile_dir ) ) {
					$uniqueTemplateFile = uniqid( md5( microtime( true ) ) ) . ".tpl";
					file_put_contents( preg_replace( "/\/$/", '', $compileDir . "/" . $uniqueTemplateFile ), $fileName );
					$returnedString = $this->PLUGIN->fetch( $compileDir . "/" . $uniqueTemplateFile );
					@unlink( $compileDir . "/" . $uniqueTemplateFile );
				}
				$this->cleanup();

				return $returnedString;
			}
		} catch ( \Exception $e ) {
			throw new \Exception( "Exception: " . __CLASS__ . "\\" . __FUNCTION__ . ": " . $e->getMessage(), $e->getCode(), $e );
		}
		$this->cleanup();
	}

	/**
	 * New evaltemplate method, should replace evalTemplate
	 *
	 * @param string $fileName
	 * @param array $variableArray
	 * @param bool $isFile
	 *
	 * @since 6.0.0
	 * @return string
	 */
	public function template( $fileName = '', $variableArray = array(), $isFile = true ) {
		return $this->evalTemplate( $fileName, $variableArray, $isFile );
	}

}
