# SmartyTemplatePluginClass for TorneLIB

## Why this dual class thing?

Back in the days TorneLIB had its own way to handle templates, inspired by a very old vBulletin release. This code could however never release, since there was a copyright issue in the code of vBulletin. When we got aware of Smarty the old function "evaltemplate" was replaced by the Smarty edition. To maintain the compatibility, Smarty became the default template handler for the global TorneLIB project, which still supported $Library->evaltemplate(); in most of the code.