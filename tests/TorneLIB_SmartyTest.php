<?php

use TorneLIB\TorneLIB_Plugin_Smarty;
use PHPUnit\Framework\TestCase;

require_once( '../vendor/autoload.php' );

class TorneLIB_SmartyTest extends TestCase {

	/** @var TorneLIB_Plugin_Smarty */
	private $PLUG;

	/** Initialize */
	function setUp() {
		$this->PLUG = new TorneLIB_Plugin_Smarty();
	}

	/** Test plogin load */
	function testPlug() {
		$this->assertTrue( is_object( $this->PLUG ) );
	}

	/** Normal template reading */
	function testEval() {
		$content = json_decode( $this->PLUG->template( "template1.txt" ) );
		$this->assertStringStartsWith( "Test test test", $content->string );
	}

	/** Evaltemplate with variables */
	function testEvalVariables() {
		$content = json_decode( $this->PLUG->template( "template2.txt", array( 'myVariable' => rand( 1024, 2048 ) ) ) );
		$this->assertTrue( intval( $content->string ) >= 1024 );
	}

	/** Call Smarty directly */
	function testPassThrough() {
		$this->PLUG->assign( array( "myVariable" => 4096 ) );
		$content = json_decode( $this->PLUG->fetch( "template3.txt" ) );
		$this->assertTrue( intval( $content->string ) >= 4096 );
	}

	function testUseProperCompileStorage() {
		$this->PLUG->setCache( "/tmp/smarty" );
		$content = json_decode( $this->PLUG->template( "template1.txt" ) );
		$this->assertStringStartsWith( "Test test test", $content->string );
	}

	function testProtectDocumentRoot() {
		$cachePath                = getcwd();
		$_SERVER['DOCUMENT_ROOT'] = getcwd();
		$this->PLUG->setCache( $cachePath . "/smarty" );
		$content = json_decode( $this->PLUG->template( "template1.txt" ) );
		$this->assertStringStartsWith( "Test test test", $content->string );
	}

}
